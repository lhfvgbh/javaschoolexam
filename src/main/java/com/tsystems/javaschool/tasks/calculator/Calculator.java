package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayList;
import java.util.List;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    List<String> stat;
    public String evaluate(String statement) {
        try{
            stat=new ArrayList<>();
            //проверка, что строка не пустая
            if(statement.equals("")||statement==null){
                return null;
            }

            int first;
            int last=1;

            //разбиение строки на отдельные числа и символы
            stat.add(""+statement.charAt(0));
            for (int i=1;i<statement.length();i++){
                if(!isAllowed(statement.charAt(i-1))){
                    return null;
                }
                if((!(Character.isDigit(statement.charAt(i-1))&& Character.isDigit(statement.charAt(i))))&& (statement.charAt(i)!=('.'))&&(statement.charAt(i-1)!=('.')) ){
                    stat.add(""+statement.charAt(i));
                }else {
                    String st = stat.get(stat.size()-1);
                    if (st.indexOf('.')!=st.length()-1){
                        String str = st+statement.charAt(i);
                        stat.remove(stat.size()-1);
                        stat.add(str);
                    }else if (statement.charAt(i)!='.'){
                        String str = st+statement.charAt(i);
                        stat.remove(stat.size()-1);
                        stat.add(str);
                    } else {
                        stat.set(0,null);
                        return null;
                    }
                }
            }

            //проверка на последовательность операций
            for (int i=1;i<stat.size()-1;i++){
                //boolean b1 = isChar(stat.get(i).charAt(0));
                //boolean b2 = isChar(stat.get(i-1).charAt(0));
                if (stat.get(i).charAt(0)==stat.get(i-1).charAt(0)){
                    stat.set(0,null);
                    return null;
                }
            }

            //проверка выражения на наличие скобок
            for (int j=0;j< stat.size();j++){
                String s = stat.get(j);
                if (s.equals("(")){
                    last=0;
                    first = stat.indexOf("(");
                    for (int i=first;i<stat.size();i++){
                        if (stat.get(i).equals("(")) {
                            first = i;
                        }
                        if (stat.get(i).equals(")")) {
                            last = stat.indexOf(")");
                            localCounter(first + 1, last - 1);
                            stat.remove("(");
                            stat.remove(")");
                        }
                    }
                }
            }
            if(last==0){
                return null;
            }

            if(stat.size()>1)
                localCounter(0,stat.size()-1);
            if(stat.get(0)!=null) {
                int res = (int)Math.round(Double.parseDouble(stat.get(0))*10000);
                double res2 = (double) res / 10000;
                int res1=(int)res2;
                if(res2-res1!=0) {
                    return Double.toString(res2);
                }else {
                    return Integer.toString(res1);
                }
            }else {
                return stat.get(0);
            }
        }catch(NullPointerException ex){
                return null;
        }
    }
    //метод вычисления выражений
    public String localCounter(int first, int last){
        Double mid;
        for (int i=first; i <= last; i++){
            if (stat.get(i).equals("*")){
                mid = Double.parseDouble(stat.get(i-1))*Double.parseDouble(stat.get(i+1));
                stat.set(i,mid.toString());
                stat.remove(i-1);
                stat.remove(i);
                last=last-2;
                i=first;
            }else if (stat.get(i).equals("/")){
                if(Double.parseDouble(stat.get(i+1))==0){
                    stat.set(0,null);
                    return null;
                }else{
                    mid = Double.parseDouble(stat.get(i-1)) / Double.parseDouble(stat.get(i + 1));
                    stat.set(i, mid.toString());
                    stat.remove(i-1);
                    stat.remove(i);
                    last=last-2;
                    i=first;
                }
            }
        }

        for (int i=first; i <= last; i++){
            if (stat.get(i).equals("+")){
                mid = Double.parseDouble(stat.get(i-1))+Double.parseDouble(stat.get(i+1));
                stat.set(i,mid.toString());
                stat.remove(i+1);
                stat.remove(i-1);
                last=last-2;
                i=first;
            }else if (stat.get(i).equals("-")){
                if(i==0){
                    mid = -Double.parseDouble(stat.get(i+1));
                    stat.set(i,mid.toString());
                    stat.remove(i+1);
                }else{
                    mid = Double.parseDouble(stat.get(i-1))-Double.parseDouble(stat.get(i+1));
                    stat.set(i,mid.toString());
                    stat.remove(i+1);
                    stat.remove(i-1);
                    last=last-2;
                    i=first;
                }

            }
        }
        return stat.get(0);
    }
    //метод проверки допустимости символов
    public boolean isAllowed(char ch){
        if(isChar(ch)||Character.isDigit(ch)){
            return true;
        }else {
            return false;
        }
    }
    //метод проверки возможных знаков
    public boolean isChar(char ch){
        if(ch=='.'||ch=='+'||ch=='*'||ch=='/'||ch=='-'||ch=='('||ch==')'){
            return true;
        }else {
            return false;
        }
    }

}
