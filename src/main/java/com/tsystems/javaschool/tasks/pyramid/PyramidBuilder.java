package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    int height = 1;
    int width = 1;
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int count = 0;
        while(count < inputNumbers.size()){
            count=count+height;
            height++;
            width=width+2;
        }
        height = height-1;
        width = width-2;
        if(inputNumbers.size()!=count||inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException("Pyramid can not be built with chosen sequence");
        }
        int[][]newPyram;
        try {
            Collections.sort(inputNumbers);

            newPyram = new int[height][width];
            //заполнение матрицы пустыми значениями
            for (int[] row : newPyram) {
                Arrays.fill(row, 0);
            }
        }catch (OutOfMemoryError e){
            throw new CannotBuildPyramidException("Pyramid can not be built: sequence is too long");
        }
        int center=(width/2);
        int countHeight=1;
        int countWidth=0;
        int move=0;
        //разделение последовательности на уровни
        for (int i=0; i<height; i++) {
            int first = center - move;
            for (int j=0; j<countHeight*2; j+=2) {
                newPyram[i][first + j] = inputNumbers.get(countWidth);
                countWidth++;
            }
            countHeight++;
            move++;
        }
        
        //вывод
        for(int[] a: newPyram){
            for(int b: a)
                System.out.print(b+" ");
            System.out.println();
        }
        return newPyram;
    }
}
