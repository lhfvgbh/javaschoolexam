package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")

    public boolean find(List x, List y) {
        int lengthX = 0;
        int lengthY = 0;
        int count = 0;
        int fit = 0;
        if(x==null||y==null) {
            throw new IllegalArgumentException("Sequence cannot be null");
        }
            lengthX = x.size();
            lengthY = y.size();
            if (lengthX > lengthY) {
                return false;
            }
            for (Object o : y) {
                if (fit < lengthX) {
                    if (o.equals(x.get(fit))) {
                        fit++;
                    } else {
                        count++;
                    }
                }
            }
        if (fit < lengthX){
            return false;
        }else{
            return true;
        }
    }
}
